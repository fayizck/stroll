import 'package:activity_ring/activity_ring.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_audio_waveforms/flutter_audio_waveforms.dart';
import 'package:flutter_chat_bubble/bubble_type.dart';
import 'package:flutter_chat_bubble/chat_bubble.dart';
import 'package:flutter_chat_bubble/clippers/chat_bubble_clipper_3.dart';
import 'package:stroll/load_audio_data.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        backgroundColor: Colors.white,
        body: Chat(),
      ),
    );
  }
}

class Chat extends StatefulWidget {
  const Chat({Key? key}) : super(key: key);

  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  late Duration maxDuration;
  late Duration elapsedDuration;
  late AudioCache audioPlayer;
  late List<double> samples;
  late int totalSamples;
  late Widget icon;
  late String iconShown;
  



  late List<String> audioData;

  List<List<String>> audioDataList = [
    [
      'assets/dm.json',
      'dance_monkey.mp3',
    ],
    [
      'assets/soy.json',
      'shape_of_you.mp3',
    ],
    [
      'assets/sp.json',
      'surface_pressure.mp3',
    ],
  ];

  Future<void> parseData() async {
    final json = await rootBundle.loadString(audioData[0]);
    Map<String, dynamic> audioDataMap = {
      "json": json,
      "totalSamples": totalSamples,
    };
    final samplesData = await compute(loadparseJson, audioDataMap);
    await audioPlayer.load(audioData[1]);
    await audioPlayer.play(audioData[1]);
    // maxDuration in milliseconds
    await Future.delayed(const Duration(milliseconds: 200));

    int maxDurationInmilliseconds =
    await audioPlayer.fixedPlayer!.getDuration();

    maxDuration = Duration(milliseconds: maxDurationInmilliseconds);
    setState(() {
      samples = samplesData["samples"];
    });
  }

  void initState() {
    // TODO: implement initState
    super.initState();
    // Change this value to number of audio samples you want.
    // Values between 256 and 1024 are good for showing [RectangleWaveform] and [SquigglyWaveform]
    // While the values above them are good for showing [PolygonWaveform]
    totalSamples = 1000;
    audioData = audioDataList[0];
    audioPlayer = AudioCache(
      fixedPlayer: AudioPlayer(),
    );
    icon=Icon(Icons.pause,color: Colors.white,);
    iconShown="pause";

    samples = [];
    maxDuration = const Duration(milliseconds: 1000);
    elapsedDuration = const Duration();
    parseData();
    audioPlayer.fixedPlayer!.onPlayerCompletion.listen((_) {
      setState(() {
        elapsedDuration = maxDuration;
        icon=Icon(Icons.replay,color: Colors.white,);
        iconShown="replay";
      });
    });
    audioPlayer.fixedPlayer!.onAudioPositionChanged
        .listen((Duration timeElapsed) {
      setState(() {
        elapsedDuration = timeElapsed;
      });
    });
  }


  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Stack(
      children: [
        Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.all(width*0.1),
                child: ChatBubble(
                  clipper: ChatBubbleClipper3(
                    type: BubbleType.sendBubble,

                  ),
                  alignment: Alignment.topRight,
                  margin: EdgeInsets.only(top: height*0.02),
                  backGroundColor: const Color(0xFF20282f),
                  child:  Padding(
                    padding:  EdgeInsets.all(height*0.01),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children:  [
                        Text(
                          "What does sports mean to you?",
                          style: TextStyle(color: Colors.white,fontWeight: FontWeight.w500),
                        ),
                        SizedBox(height: height*0.02),
                        Row(
                          children: [
                            Flexible(
                              child: InkWell(
                                  onTap: (){
                                    if(iconShown=="pause")
                                    {
                                      audioPlayer.fixedPlayer!.pause();
                                      setState(() {
                                        iconShown="play";
                                        icon=Icon(CupertinoIcons.play_arrow_solid,color: Colors.white,);
                                      });

                                    }else if(iconShown=="play"){
                                      audioPlayer.fixedPlayer!.resume();
                                      setState(() {
                                        iconShown="pause";
                                        icon=Icon(Icons.pause,color: Colors.white,);
                                      });

                                    }else{
                                      audioPlayer.fixedPlayer!
                                          .seek(const Duration(milliseconds: 0));
                                      audioPlayer.fixedPlayer!.resume();
                                      setState(() {
                                        iconShown="pause";
                                        icon=Icon(Icons.pause,color: Colors.white,);
                                      });
                                    }

                                  },
                                  child: icon),
                            ),
                            Expanded(child: SquigglyWaveform(
                              activeColor: Color(0xff4b4d87),
                              inactiveColor: Color(0xff747696),
                              maxDuration: maxDuration,
                              elapsedDuration: elapsedDuration,
                              samples: samples,
                              height: height*0.04,
                              width: width*0.336,
                            ),)

                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              "${elapsedDuration.inMinutes}:${elapsedDuration.inSeconds.toString().padLeft(2, '0')}",
                              style: TextStyle(color: Colors.white),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),

        Align(
          alignment: Alignment.centerRight,
          child: Padding(
            padding:  EdgeInsets.only(top: height*0.13,right: width*0.025),
            child: Container(
              child: CustomPaint(
                size: Size(width*0.13, width*0.13),
                painter: CirclePainter(),
              ),
            ),
          ),
        ),

      ],
    );
  }
}


class CirclePainter extends CustomPainter {
  final _paint = Paint()
    ..color = Color(0xFF20282f)
    ..strokeWidth = 4
  // Use [PaintingStyle.fill] if you want the circle to be filled.
    ..style = PaintingStyle.stroke;

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawOval(
      Rect.fromLTWH(0, 0, size.width, size.height),
      _paint,
    );
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

